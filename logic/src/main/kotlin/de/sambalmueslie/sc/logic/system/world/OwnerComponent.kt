package de.sambalmueslie.sc.logic.system.world

import de.sambalmueslie.sc.logic.ecs.common.Component

data class OwnerComponent(
    val playerName: String
) : Component
