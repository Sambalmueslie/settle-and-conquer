package de.sambalmueslie.sc.logic.main


import de.sambalmueslie.sc.logic.ecs.GraphicEngine
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class SampleGraphicEngine : GraphicEngine {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(SampleGraphicEngine::class.java)
    }

    override fun render() {
        logger.info("Render")
    }

}
