package de.sambalmueslie.sc.logic.ecs.engine

import de.sambalmueslie.sc.logic.ecs.common.Processable
import de.sambalmueslie.sc.logic.ecs.common.System

interface LogicEngine: Processable {
    fun register(system: System)
}
