package de.sambalmueslie.sc.logic.system.construction

import de.sambalmueslie.sc.logic.ecs.common.Entity
import de.sambalmueslie.sc.logic.ecs.event.EntityChangeEvent

data class ConstructionChangeEvent(
    override val entity: Entity,
    val component: BuildingComponent,
    val finalLevel: Int,
    val ongoing: Boolean,
    val remainingMillis: Long
) : EntityChangeEvent
