package de.sambalmueslie.sc.logic.ecs.event

interface ChangeEventConsumer<T: ChangeEvent> {
    fun handleEvent(event: T)
}
