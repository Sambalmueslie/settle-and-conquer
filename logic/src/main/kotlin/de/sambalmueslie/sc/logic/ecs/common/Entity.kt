package de.sambalmueslie.sc.logic.ecs.common

interface Entity {
    val id: Long
    val name: String
}
