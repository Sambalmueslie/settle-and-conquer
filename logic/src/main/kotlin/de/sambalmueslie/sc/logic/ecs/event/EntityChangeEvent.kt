package de.sambalmueslie.sc.logic.ecs.event

import de.sambalmueslie.sc.logic.ecs.common.Entity


interface EntityChangeEvent : ChangeEvent {
    val entity: Entity
}
