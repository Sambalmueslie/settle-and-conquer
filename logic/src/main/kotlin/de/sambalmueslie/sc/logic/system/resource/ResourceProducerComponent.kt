package de.sambalmueslie.sc.logic.system.resource

import de.sambalmueslie.sc.logic.ecs.common.Component

data class ResourceProducerComponent(
    val resourceName: String,
    val buildingName: String,
    var production: Double
) : Component
