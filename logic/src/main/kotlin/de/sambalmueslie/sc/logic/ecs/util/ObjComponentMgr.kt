package de.sambalmueslie.sc.logic.ecs.util

import de.sambalmueslie.sc.logic.ecs.common.Component
import de.sambalmueslie.sc.logic.ecs.common.GameObject
import kotlin.reflect.KClass


class ObjComponentMgr : GameObject {
    private val components: MutableMap<KClass<*>, Component> = mutableMapOf()

    override fun hasComponent(type: KClass<*>): Boolean {
        return components.containsKey(type)
    }

    override fun get(type: KClass<*>): Component? {
        return components[type]
    }

    override fun add(component: Component) {
        components[component::class] = component
    }
}
