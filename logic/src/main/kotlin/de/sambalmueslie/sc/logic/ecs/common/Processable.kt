package de.sambalmueslie.sc.logic.ecs.common

interface Processable {
    fun process(dt: Long)
}
