package de.sambalmueslie.sc.logic.system.world

import de.sambalmueslie.sc.logic.ecs.common.System
import de.sambalmueslie.sc.logic.ecs.entity.EntityManager
import de.sambalmueslie.sc.logic.ecs.event.ChangeEventPublisher
import de.sambalmueslie.sc.logic.ecs.event.ChangeEventRegistrar
import de.sambalmueslie.sc.logic.system.common.SystemEventProcessor
import de.sambalmueslie.sc.logic.system.construction.BuildingCreateRequest
import de.sambalmueslie.sc.logic.system.movement.PositionChangeRequest
import de.sambalmueslie.sc.logic.system.resource.ResourceAmountChangeRequest
import de.sambalmueslie.sc.logic.system.resource.ResourceProductionChangeRequest
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class WorldSystem(private val entityManager: EntityManager) : System {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(WorldSystem::class.java)
    }

    private lateinit var villageEventPublisher: ChangeEventPublisher<VillageChangeEvent>
    private lateinit var buildingEventPublisher: ChangeEventPublisher<BuildingCreateRequest>
    private lateinit var resourceAmountEventPublisher: ChangeEventPublisher<ResourceAmountChangeRequest>
    private lateinit var resourceProductionEventPublisher: ChangeEventPublisher<ResourceProductionChangeRequest>
    private lateinit var positionEventPublisher: ChangeEventPublisher<PositionChangeRequest>
    private var villageCtr = 0


    private val processors = listOf(
        SystemEventProcessor(CreatePlayerRequest::class, this::process, false)
    )

    override fun setup(registrar: ChangeEventRegistrar) {
        processors.forEach { it.register(registrar) }

        villageEventPublisher = registrar.getPublisher(VillageChangeEvent::class)
        buildingEventPublisher = registrar.getPublisher(BuildingCreateRequest::class)
        resourceAmountEventPublisher = registrar.getPublisher(ResourceAmountChangeRequest::class)
        resourceProductionEventPublisher = registrar.getPublisher(ResourceProductionChangeRequest::class)
        positionEventPublisher = registrar.getPublisher(PositionChangeRequest::class)
    }

    override fun process(dt: Long) {
        processors.forEach { it.process() }
    }


    private fun process(request: CreatePlayerRequest): Boolean {
        val villageName = "Village ${++villageCtr}"
        val components = listOf(OwnerComponent(request.playerName))
        val villageEntity = entityManager.createEntity(villageName, components)

        // TODO determine position
        positionEventPublisher.publish(PositionChangeRequest(villageEntity, listOf(0, 0, 0)))

        // TODO get that from config file
        val resourceAmountRequests = listOf(
            ResourceAmountChangeRequest(villageEntity, "Wood", 1000, true),
            ResourceAmountChangeRequest(villageEntity, "Stone", 1000, true),
            ResourceAmountChangeRequest(villageEntity, "Coal", 200, true),
            ResourceAmountChangeRequest(villageEntity, "Iron", 200, true),
            ResourceAmountChangeRequest(villageEntity, "Gold", 200, true)
        )
        resourceAmountRequests.forEach(resourceAmountEventPublisher::publish)

        // TODO get that from config file
        val resourceProductionRequests = listOf(
            ResourceProductionChangeRequest(villageEntity, "Wood", "Lumberjack", 10.0),
            ResourceProductionChangeRequest(villageEntity, "Stone", "Quarry", 8.0),
            ResourceProductionChangeRequest(villageEntity, "Coal", "Coal Mine", 3.0),
            ResourceProductionChangeRequest(villageEntity, "Iron", "Iron Mine", 2.0),
            ResourceProductionChangeRequest(villageEntity, "Gold", "Gold Mine", 0.5)
        )
        resourceProductionRequests.forEach(resourceProductionEventPublisher::publish)

        // TODO get that from config file
        val buildingRequests = listOf(
            BuildingCreateRequest(villageEntity, "Town Hall", 1, 5),
            BuildingCreateRequest(villageEntity, "Lumberjack", 1, 30),
            BuildingCreateRequest(villageEntity, "Quarry", 1, 30),
            BuildingCreateRequest(villageEntity, "Coal Mine", 1, 20),
            BuildingCreateRequest(villageEntity, "Iron Mine", 1, 20),
            BuildingCreateRequest(villageEntity, "Gold Mine", 1, 20)
        )
        buildingRequests.forEach(buildingEventPublisher::publish)

        villageEventPublisher.publish(VillageChangeEvent(villageEntity))
        return true
    }


}
