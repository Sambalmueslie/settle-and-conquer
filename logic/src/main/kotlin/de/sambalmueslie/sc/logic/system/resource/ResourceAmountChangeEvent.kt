package de.sambalmueslie.sc.logic.system.resource

import de.sambalmueslie.sc.logic.ecs.common.Entity
import de.sambalmueslie.sc.logic.ecs.event.EntityChangeEvent

data class ResourceAmountChangeEvent(
    override val entity: Entity,
    val component: ResourceAmountComponent,
    val formerAmount: Int
) : EntityChangeEvent
