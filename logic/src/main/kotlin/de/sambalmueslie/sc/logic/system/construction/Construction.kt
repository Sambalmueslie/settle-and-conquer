package de.sambalmueslie.sc.logic.system.construction

data class Construction(
    val id: String,
    val entityId: Long,
    val component: BuildingComponent,
    val finalLevel: Int,
    val duration: Long,
    val startTimestamp: Long,
    val endTimestamp: Long
)
