package de.sambalmueslie.sc.logic.ecs.event


import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.reflect.KClass

class MessageQueue<T: ChangeEvent>(private val type: KClass<T>) {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(MessageQueue::class.java)
    }

    private val name = type.simpleName
    private val consumers: MutableSet<ChangeEventConsumer<T>> = mutableSetOf()
    val publisher: ChangeEventPublisher<T> = MessagePublisher(consumers)

    fun subscribe(consumer: ChangeEventConsumer<T>) {
        logger.debug("[$name] subscribe $consumer")
        consumers.add(consumer)
    }

    fun unsubscribe(consumer: ChangeEventConsumer<*>) {
        logger.debug("[$name] unsubscribe $consumer")
        consumers.remove(consumer)
    }

}
