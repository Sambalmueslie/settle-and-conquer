package de.sambalmueslie.sc.logic.ecs.event

import org.slf4j.Logger
import org.slf4j.LoggerFactory

class MessagePublisher<T: ChangeEvent>(private val consumers: Set<ChangeEventConsumer<T>>) : ChangeEventPublisher<T> {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(MessagePublisher::class.java)
    }

    override fun publish(event: T) {
        logger.info("Publish event $event")
        consumers.forEach { it.handleEvent(event) }
    }
}
