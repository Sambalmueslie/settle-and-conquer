package de.sambalmueslie.sc.logic.main


import de.sambalmueslie.sc.logic.ecs.common.System
import de.sambalmueslie.sc.logic.ecs.event.ChangeEvent
import de.sambalmueslie.sc.logic.ecs.event.ChangeEventConsumer
import de.sambalmueslie.sc.logic.ecs.event.ChangeEventRegistrar
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class SampleConsumingSystem : System {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(SampleConsumingSystem::class.java)
    }

    override fun setup(registrar: ChangeEventRegistrar) {
        registrar.subscribe(SampleEvent::class, object : ChangeEventConsumer<SampleEvent> {
            override fun handleEvent(event: SampleEvent) {
                logger.info("Handle event")
            }
        })
    }

    override fun process(dt: Long) {
    }



}
