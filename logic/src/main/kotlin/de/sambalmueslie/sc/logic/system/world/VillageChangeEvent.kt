package de.sambalmueslie.sc.logic.system.world

import de.sambalmueslie.sc.logic.ecs.common.Entity
import de.sambalmueslie.sc.logic.ecs.event.EntityChangeEvent

data class VillageChangeEvent(
    override val entity: Entity
) : EntityChangeEvent
