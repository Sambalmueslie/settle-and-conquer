package de.sambalmueslie.sc.logic.system.common


import de.sambalmueslie.sc.logic.ecs.event.ChangeEvent
import de.sambalmueslie.sc.logic.ecs.event.ChangeEventConsumer
import de.sambalmueslie.sc.logic.ecs.event.ChangeEventRegistrar
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.math.max
import kotlin.reflect.KClass

class SystemEventProcessor<T : ChangeEvent>(
    private val type: KClass<T>,
    private val callback: (T) -> Boolean,
private val awaiting: Boolean = false
) {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(SystemEventProcessor::class.java)
    }

    private val events: MutableList<T> = mutableListOf()

    fun register(registrar: ChangeEventRegistrar) {
        registrar.subscribe(type, object : ChangeEventConsumer<T> {
            override fun handleEvent(event: T) {
                events.add(event)
            }
        })
    }

    fun process() {
        when (awaiting) {
            true -> processAwaiting()
            else -> processRemoving()
        }
    }

    private fun processAwaiting() {
        val iterator = events.listIterator()
        while (iterator.hasNext()) {
            val event = iterator.next()
            val result = callback.invoke(event)
            if (result) {
                iterator.remove()
            }
        }
    }

    private fun processRemoving() {
        events.forEach { callback.invoke(it) }
        events.clear()
    }

}
