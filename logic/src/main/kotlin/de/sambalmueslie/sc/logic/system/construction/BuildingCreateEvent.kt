package de.sambalmueslie.sc.logic.system.construction

import de.sambalmueslie.sc.logic.ecs.common.Entity
import de.sambalmueslie.sc.logic.ecs.event.EntityChangeEvent

data class BuildingCreateEvent(
    override val entity: Entity,
    val component: BuildingComponent
) : EntityChangeEvent
