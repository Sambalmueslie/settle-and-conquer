package de.sambalmueslie.sc.logic.system.movement

import de.sambalmueslie.sc.logic.ecs.common.Component

data class PositionComponent(
    var coordinate: List<Int> = listOf(0, 0),
    val movable: Boolean
) : Component {

}
