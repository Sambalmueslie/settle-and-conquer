package de.sambalmueslie.sc.logic.system.resource

import de.sambalmueslie.sc.logic.ecs.common.Entity
import de.sambalmueslie.sc.logic.ecs.event.EntityChangeEvent

data class ResourceProductionChangeRequest(
    override val entity: Entity,
    val resourceName: String,
    val buildingName: String,
    var production: Double
) : EntityChangeEvent
