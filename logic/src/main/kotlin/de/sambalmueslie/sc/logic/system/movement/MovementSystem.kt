package de.sambalmueslie.sc.logic.system.movement


import de.sambalmueslie.sc.logic.ecs.common.Entity
import de.sambalmueslie.sc.logic.ecs.common.System
import de.sambalmueslie.sc.logic.ecs.entity.EntityManager
import de.sambalmueslie.sc.logic.ecs.event.ChangeEventConsumer
import de.sambalmueslie.sc.logic.ecs.event.ChangeEventPublisher
import de.sambalmueslie.sc.logic.ecs.event.ChangeEventRegistrar
import de.sambalmueslie.sc.logic.system.common.SystemEventProcessor
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class MovementSystem(private val entityManager: EntityManager) : System {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(MovementSystem::class.java)
    }

    private val processors = listOf(
        SystemEventProcessor(PositionChangeRequest::class, this::process, false)
    )

    private lateinit var changeEventPublisher: ChangeEventPublisher<PositionChangeEvent>

    override fun setup(registrar: ChangeEventRegistrar) {
        processors.forEach { it.register(registrar) }

        changeEventPublisher = registrar.getPublisher(PositionChangeEvent::class)
    }

    override fun process(dt: Long) {
        processors.forEach { it.process() }
    }


    private fun process(request: PositionChangeRequest): Boolean {
        val components = entityManager.getComponents(request.entity, PositionComponent::class)
        if (components.isEmpty()) {
            createNewPositionComponent(request)
        } else {
            updatePositionComponents(request, components)
        }
        return true
    }

    private fun updatePositionComponents(request: PositionChangeRequest, components: List<PositionComponent>) {
        if (request.initial) return
        val movableComponents = components.filter { it.movable }
        movableComponents.forEach { setValue(request.entity, it, request.change) }
    }

    private fun setValue(entity: Entity, component: PositionComponent, value: List<Int>) {
        val formerPosition = component.coordinate
        component.coordinate = value
        changeEventPublisher.publish(PositionChangeEvent(entity, component, formerPosition))
    }

    private fun createNewPositionComponent(request: PositionChangeRequest) {
        if (!request.initial) return
        val component = PositionComponent(request.change, request.movable)
        val entity = request.entity
        entityManager.addComponent(entity, component) ?: return
        changeEventPublisher.publish(PositionChangeEvent(entity, component, emptyList()))
    }


}
