package de.sambalmueslie.sc.logic.main

import de.sambalmueslie.sc.logic.ecs.engine.GameLogicEngine
import de.sambalmueslie.sc.logic.ecs.entity.GameEntityManager
import de.sambalmueslie.sc.logic.ecs.runner.GameRunner
import de.sambalmueslie.sc.logic.system.construction.BuildingComponent
import de.sambalmueslie.sc.logic.system.construction.ConstructionSystem
import de.sambalmueslie.sc.logic.system.movement.MovementSystem
import de.sambalmueslie.sc.logic.system.resource.ResourceSystem
import de.sambalmueslie.sc.logic.system.world.CreatePlayerRequest
import de.sambalmueslie.sc.logic.system.world.WorldSystem

fun main() {
    val entityManager = GameEntityManager()



    val logicEngine = GameLogicEngine(entityManager)
    val graphicEngine = SampleGraphicEngine()

    logicEngine.register(MovementSystem(entityManager))
    logicEngine.register(ConstructionSystem(entityManager))
    logicEngine.register(WorldSystem(entityManager))
    logicEngine.register(ResourceSystem(entityManager))

    logicEngine.publish(CreatePlayerRequest("Player 1"))

    val runner = GameRunner(logicEngine, graphicEngine, 1, 5000)
    runner.run()
}
