package de.sambalmueslie.sc.logic.system.resource

import de.sambalmueslie.sc.logic.ecs.common.Entity
import de.sambalmueslie.sc.logic.ecs.event.EntityChangeEvent

data class ResourceProductionChangeEvent(
    override val entity: Entity,
    val component: ResourceProducerComponent,
    val formerProduction: Double
) : EntityChangeEvent
