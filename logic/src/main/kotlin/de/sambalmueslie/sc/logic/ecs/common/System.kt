package de.sambalmueslie.sc.logic.ecs.common

import de.sambalmueslie.sc.logic.ecs.common.Processable
import de.sambalmueslie.sc.logic.ecs.event.ChangeEventConsumer
import de.sambalmueslie.sc.logic.ecs.event.ChangeEventRegistrar

interface System: Processable {
    fun setup(registrar: ChangeEventRegistrar)
}
