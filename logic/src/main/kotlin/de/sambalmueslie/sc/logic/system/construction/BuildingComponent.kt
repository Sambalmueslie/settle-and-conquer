package de.sambalmueslie.sc.logic.system.construction


import de.sambalmueslie.sc.logic.ecs.common.Component
import de.sambalmueslie.sc.logic.system.common.Expansion

data class BuildingComponent(
    val name: String,
    var level: Int,
    var minLevel: Int,
    var maxLevel: Int,
    var expansion: Expansion
) : Component
