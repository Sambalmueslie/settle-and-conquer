package de.sambalmueslie.sc.logic.ecs.event

import kotlin.reflect.KClass

interface ChangeEventRegistrar {
    fun <T : ChangeEvent> subscribe(queue: KClass<T>, consumer: ChangeEventConsumer<T>)
    fun <T : ChangeEvent> unsubscribe(queue: KClass<T>, consumer: ChangeEventConsumer<T>)
    fun unsubscribe(consumer: ChangeEventConsumer<*>)
    fun <T : ChangeEvent> getPublisher(queue: KClass<T>): ChangeEventPublisher<T>
}
