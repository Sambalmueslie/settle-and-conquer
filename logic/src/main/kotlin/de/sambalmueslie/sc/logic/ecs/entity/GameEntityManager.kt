package de.sambalmueslie.sc.logic.ecs.entity


import de.sambalmueslie.sc.logic.ecs.common.Component
import de.sambalmueslie.sc.logic.ecs.common.Entity
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.reflect.KClass

class GameEntityManager : EntityManager {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(GameEntityManager::class.java)
    }


    private val entities: MutableMap<Long, GameEntity> = mutableMapOf()
    private var idCtr: Long = 0

    override fun getAll(): List<Entity> {
        return entities.values.toList()
    }

    override fun createEntity(name: String, components: List<Component>): Entity {
        val id = idCtr++
        val entity = GameEntity(id, name)
        entity.addComponents(components)
        logger.info("Create new entity $entity")
        entities[id] = entity
        return entity
    }

    override fun getEntity(entityId: Long): Entity? {
        return entities[entityId]
    }

    override fun <T : Component> getComponents(entityId: Long, type: KClass<T>): List<T> {
        val entity = entities[entityId] ?: return emptyList()
        return entity.getComponents(type)
    }

    override fun <T : Component> getComponents(entity: Entity, type: KClass<T>): List<T> {
        return getComponents(entity.id, type)
    }

    override fun <T : Component> addComponent(entityId: Long, component: T): T? {
        val entity = entities[entityId] ?: return null
        return entity.addComponent(component)
    }

    override fun <T : Component> addComponent(entity: Entity, component: T): T? {
        return addComponent(entity.id, component)
    }

    override fun <T : Component> addComponents(entity: Entity, components: List<T>): List<T> {
        return addComponents(entity.id, components)
    }

    override fun <T : Component> addComponents(entityId: Long, components: List<T>): List<T> {
        val entity = entities[entityId] ?: return emptyList()
        return entity.addComponents(components)
    }


}
