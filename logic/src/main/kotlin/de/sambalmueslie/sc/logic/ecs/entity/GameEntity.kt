package de.sambalmueslie.sc.logic.ecs.entity


import de.sambalmueslie.sc.logic.ecs.common.Component
import de.sambalmueslie.sc.logic.ecs.common.Entity
import kotlin.reflect.KClass

data class GameEntity(
    override val id: Long,
    override val name: String,
    private val components: MutableList<Component> = mutableListOf()
) : Entity {

    @Suppress("UNCHECKED_CAST")
    fun <T : Component> getComponents(type: KClass<T>): List<T> {
        val result = components.filter { it::class == type }
        return result as List<T>
    }

    fun <T : Component> addComponent(component: T): T? {
        components.add(component)
        return component
    }

    fun <T : Component> removeComponent(component: T) {
        components.remove(component)
    }

    fun <T : Component> addComponents(comps: List<T>): List<T> {
        components.addAll(comps)
        return comps
    }
}
