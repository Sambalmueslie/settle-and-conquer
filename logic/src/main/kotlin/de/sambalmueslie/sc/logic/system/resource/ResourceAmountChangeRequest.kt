package de.sambalmueslie.sc.logic.system.resource

import de.sambalmueslie.sc.logic.ecs.common.Entity
import de.sambalmueslie.sc.logic.ecs.event.EntityChangeEvent

data class ResourceAmountChangeRequest(
    override val entity: Entity,
    val resourceName: String,
    val valueToChange: Int,
    val initial: Boolean = false
) : EntityChangeEvent
