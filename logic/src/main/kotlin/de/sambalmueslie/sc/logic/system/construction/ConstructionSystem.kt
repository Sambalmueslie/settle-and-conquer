package de.sambalmueslie.sc.logic.system.construction

import de.sambalmueslie.sc.logic.ecs.common.System
import de.sambalmueslie.sc.logic.ecs.entity.EntityManager
import de.sambalmueslie.sc.logic.ecs.event.ChangeEventConsumer
import de.sambalmueslie.sc.logic.ecs.event.ChangeEventPublisher
import de.sambalmueslie.sc.logic.ecs.event.ChangeEventRegistrar
import de.sambalmueslie.sc.logic.system.common.SystemEventProcessor
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class ConstructionSystem(private val entityManager: EntityManager) : System {


    companion object {
        val logger: Logger = LoggerFactory.getLogger(ConstructionSystem::class.java)
    }

    private val constructionMgr = ConstructionMgr(entityManager)
    private val processors = listOf(
        SystemEventProcessor(ConstructionChangeRequest::class, this::process, true),
        SystemEventProcessor(BuildingCreateRequest::class, this::process, false)
    )

    private lateinit var constructionEventPublisher: ChangeEventPublisher<ConstructionChangeEvent>
    private lateinit var buildingEventPublisher: ChangeEventPublisher<BuildingCreateEvent>



    override fun setup(registrar: ChangeEventRegistrar) {
        processors.forEach { it.register(registrar) }

        constructionEventPublisher = registrar.getPublisher(ConstructionChangeEvent::class)
        buildingEventPublisher = registrar.getPublisher(BuildingCreateEvent::class)
    }


    override fun process(dt: Long) {
        processors.forEach { it.process() }
        processUpdates()
    }

    private fun process(req: ConstructionChangeRequest): Boolean {
        val components = entityManager.getComponents(req.entity, BuildingComponent::class)
        val component = components.find { it.name == req.buildingName } ?: return true
        return constructionMgr.startConstruction(req.entity, component, req.initial)
    }

    private fun process(req: BuildingCreateRequest): Boolean {
        val components = entityManager.getComponents(req.entity, BuildingComponent::class)
        val existing = components.find { it.name == req.buildingName }
        val comp = if (existing == null) {
            val c = BuildingComponent(req.buildingName, req.minLevel, req.minLevel, req.maxLevel, req.expansion)
            entityManager.addComponent(req.entity, c)
            c
        } else {
            existing.minLevel = req.minLevel
            existing.maxLevel = req.maxLevel
            existing.expansion = req.expansion
            existing.level = determineLevel(existing, req)
            existing
        }
        buildingEventPublisher.publish(BuildingCreateEvent(req.entity, comp))
        return true
    }

    private fun determineLevel(existing: BuildingComponent, req: BuildingCreateRequest): Int {
        return when {
            existing.level in req.minLevel..req.maxLevel -> existing.level
            existing.level < req.minLevel -> req.minLevel
            else -> req.maxLevel
        }
    }


    private fun processUpdates() {
        val events = constructionMgr.processUpdates()
        events.forEach { constructionEventPublisher.publish(it) }
    }



}
