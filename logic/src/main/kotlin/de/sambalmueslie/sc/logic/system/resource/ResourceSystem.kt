package de.sambalmueslie.sc.logic.system.resource

import de.sambalmueslie.sc.logic.ecs.common.Entity
import de.sambalmueslie.sc.logic.ecs.common.System
import de.sambalmueslie.sc.logic.ecs.entity.EntityManager
import de.sambalmueslie.sc.logic.ecs.event.ChangeEventPublisher
import de.sambalmueslie.sc.logic.ecs.event.ChangeEventRegistrar
import de.sambalmueslie.sc.logic.system.common.SystemEventProcessor
import de.sambalmueslie.sc.logic.system.construction.ConstructionChangeEvent
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class ResourceSystem(private val entityManager: EntityManager) : System {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(ResourceSystem::class.java)
        const val RESOURCE_UPDATE_PERIOD = 10
    }



    private val processors = listOf(
        SystemEventProcessor(ResourceAmountChangeRequest::class, this::process, false),
        SystemEventProcessor(ResourceProductionChangeRequest::class, this::process, false),
        SystemEventProcessor(ConstructionChangeEvent::class, this::process, false)
    )

    private lateinit var changeAmountPublisher: ChangeEventPublisher<ResourceAmountChangeEvent>
    private lateinit var changeProducerPublisher: ChangeEventPublisher<ResourceProductionChangeEvent>

    override fun setup(registrar: ChangeEventRegistrar) {
        processors.forEach { it.register(registrar) }
        changeAmountPublisher = registrar.getPublisher(ResourceAmountChangeEvent::class)
        changeProducerPublisher = registrar.getPublisher(ResourceProductionChangeEvent::class)
    }

    private var ctr: Long = 0

    override fun process(dt: Long) {
        processors.forEach { it.process() }

        ctr += dt
        if (ctr >= RESOURCE_UPDATE_PERIOD * 1000) {
            processVillageResourceChanges()
            ctr = 0
        }
    }


    private fun process(req: ResourceAmountChangeRequest): Boolean {
        val villageEntity = req.entity
        val comps = entityManager.getComponents(villageEntity, ResourceAmountComponent::class)
        val c = comps.filter { it.resourceName == req.resourceName }
        when {
            c.isEmpty() && req.initial -> create(req)
            c.isNotEmpty() -> c.forEach { setValue(req, it) }
        }
        return true
    }

    private fun create(req: ResourceAmountChangeRequest) {
        val component = ResourceAmountComponent(req.resourceName, req.valueToChange)
        entityManager.addComponent(req.entity, component)
        changeAmountPublisher.publish(ResourceAmountChangeEvent(req.entity, component, 0))
    }

    private fun setValue(req: ResourceAmountChangeRequest, component: ResourceAmountComponent) {
        val formerValue = component.amount
        if (req.initial) {
            component.amount = req.valueToChange
        } else {
            component.amount += req.valueToChange
        }
        changeAmountPublisher.publish(ResourceAmountChangeEvent(req.entity, component, formerValue))
    }

    private fun process(req: ResourceProductionChangeRequest): Boolean {
        val villageEntity = req.entity
        val comps = entityManager.getComponents(villageEntity, ResourceProducerComponent::class)
        val c = comps.filter { it.resourceName == req.resourceName }
        when {
            c.isEmpty() -> create(req)
            c.isNotEmpty() -> c.forEach { setValue(req, it) }
        }
        return true
    }

    private fun create(req: ResourceProductionChangeRequest) {
        val component = ResourceProducerComponent(req.resourceName, req.buildingName, req.production)
        entityManager.addComponent(req.entity, component)
        changeProducerPublisher.publish(ResourceProductionChangeEvent(req.entity, component, 0.0))
    }

    private fun setValue(req: ResourceProductionChangeRequest, component: ResourceProducerComponent) {
        val formerValue = component.production
        component.production = req.production
        changeProducerPublisher.publish(ResourceProductionChangeEvent(req.entity, component, formerValue))
    }

    private fun process(evt: ConstructionChangeEvent): Boolean {
        if (evt.ongoing) return true

        val entity = evt.entity
        val components = entityManager.getComponents(entity, ResourceProducerComponent::class)
        if (components.isEmpty()) return true

        val buildingName = evt.component.name
        val comps = components.filter { it.buildingName == buildingName }
        if (comps.isEmpty()) return true

        comps.forEach { setValue(evt, it) }
        return true
    }

    private fun setValue(evt: ConstructionChangeEvent, component: ResourceProducerComponent) {
        val formerValue = component.production
        // TODO create some useful expansion
        component.production = component.production * 2
        changeProducerPublisher.publish(ResourceProductionChangeEvent(evt.entity, component, formerValue))
    }


    private fun processVillageResourceChanges() {
        val entities = entityManager.getAll()
        entities.forEach { process(it) }
    }

    private fun process(entity: Entity) {
        val prodComp = entityManager.getComponents(entity, ResourceProducerComponent::class)
            .map { it.resourceName to it }.toMap()
        if (prodComp.isEmpty()) return

        val resComp = entityManager.getComponents(entity, ResourceAmountComponent::class)
            .map { it.resourceName to it }.toMap()
        if (resComp.isEmpty()) return

        prodComp.mapNotNull { p -> resComp[p.key]?.let { p.value to it } }
            .forEach { updateValue(entity, it.first, it.second) }
    }

    private fun updateValue(entity: Entity, producer: ResourceProducerComponent, amount: ResourceAmountComponent) {
        val formerValue = amount.amount
        val change = producer.production * RESOURCE_UPDATE_PERIOD
        amount.amount += change.toInt()
        changeAmountPublisher.publish(ResourceAmountChangeEvent(entity, amount, formerValue))
    }

}
