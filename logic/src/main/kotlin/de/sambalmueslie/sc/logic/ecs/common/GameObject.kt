package de.sambalmueslie.sc.logic.ecs.common

import kotlin.reflect.KClass

interface GameObject {
    fun hasComponent(type: KClass<*>): Boolean
    fun get(type: KClass<*>): Component?
    fun add(component: Component)
}
