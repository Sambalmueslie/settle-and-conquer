package de.sambalmueslie.sc.logic.ecs.engine


import de.sambalmueslie.sc.logic.ecs.common.System
import de.sambalmueslie.sc.logic.ecs.entity.EntityManager
import de.sambalmueslie.sc.logic.ecs.entity.GameEntityManager
import de.sambalmueslie.sc.logic.ecs.event.ChangeEvent
import de.sambalmueslie.sc.logic.ecs.event.ChangeEventPublisher
import de.sambalmueslie.sc.logic.ecs.event.MessageBroker
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class GameLogicEngine(private val entityManager: EntityManager) : LogicEngine {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(GameLogicEngine::class.java)
    }

    private val systems: MutableList<System> = mutableListOf()
    private val broker: MessageBroker = MessageBroker()

    override fun register(system: System) {
        logger.info("Register system $system")
        systems.add(system)
        system.setup(broker)
    }

    override fun process(dt: Long) {
        logger.debug("Process $dt for ${systems.size} systems.")
        systems.forEach { it.process(dt) }
    }


    fun <T: ChangeEvent> publish(event: T) {
        val publisher = broker.getPublisher(event::class) as ChangeEventPublisher<T>
        publisher.publish(event)
    }


}
