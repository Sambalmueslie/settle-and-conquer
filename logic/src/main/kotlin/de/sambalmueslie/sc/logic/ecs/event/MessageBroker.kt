package de.sambalmueslie.sc.logic.ecs.event


import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.reflect.KClass

class MessageBroker : ChangeEventRegistrar {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(MessageBroker::class.java)
    }

    private val queues: MutableMap<KClass<*>, MessageQueue<*>> = mutableMapOf()

    override fun <T : ChangeEvent> getPublisher(queue: KClass<T>): ChangeEventPublisher<T> {
        val q = getQueue(queue)
        return q.publisher
    }

    override fun <T : ChangeEvent> subscribe(queue: KClass<T>, consumer: ChangeEventConsumer<T>) {
        val q= getQueue(queue)
        q.subscribe(consumer)
    }

    override fun <T : ChangeEvent> unsubscribe(queue: KClass<T>, consumer: ChangeEventConsumer<T>) {
        val q= getQueue(queue)
        q.unsubscribe(consumer)
    }

    override fun unsubscribe(consumer: ChangeEventConsumer<*>) {
        logger.info("Unsubscribe consumer $consumer")
        queues.values.forEach { it.unsubscribe(consumer) }
    }

    @Suppress("UNCHECKED_CAST")
    private fun <T : ChangeEvent> getQueue(eventType: KClass<T>): MessageQueue<T> {
        val existing = queues[eventType]
        if (existing != null) return existing as MessageQueue<T>

        val queue = MessageQueue(eventType)
        queues[eventType] = queue
        return queue
    }



}
