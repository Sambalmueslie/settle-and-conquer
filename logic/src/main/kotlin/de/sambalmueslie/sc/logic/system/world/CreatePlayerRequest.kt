package de.sambalmueslie.sc.logic.system.world

import de.sambalmueslie.sc.logic.ecs.event.ChangeEvent

data class CreatePlayerRequest(
    val playerName: String
) : ChangeEvent
