package de.sambalmueslie.sc.logic.system.construction

import de.sambalmueslie.sc.logic.ecs.common.Entity
import de.sambalmueslie.sc.logic.ecs.event.EntityChangeEvent

data class ConstructionChangeRequest(
    override val entity: Entity,
    val buildingName: String,
    val requestedLevel: Int,
    val initial: Boolean = false
) : EntityChangeEvent
