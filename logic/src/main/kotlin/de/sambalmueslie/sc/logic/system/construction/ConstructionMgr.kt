package de.sambalmueslie.sc.logic.system.construction

import de.sambalmueslie.sc.logic.ecs.common.Entity
import de.sambalmueslie.sc.logic.ecs.entity.EntityManager

internal class ConstructionMgr(private val entityManager: EntityManager) {

    private val constructions: MutableMap<String, Construction> = mutableMapOf()


    fun startConstruction(entity: Entity, component: BuildingComponent, initial: Boolean): Boolean {
        val constructionId = getId(entity, component)
        if (constructions.containsKey(constructionId)) return false

        val finalLevel = component.level + 1
        if(finalLevel >= component.maxLevel) return true

        val duration: Long = (component.expansion.getValue(finalLevel) * 1000).toLong()
        // TODO use some internal ticks
        val startTime = System.currentTimeMillis()
        val endTime = if (initial) startTime else startTime + duration
        val construction = Construction(constructionId, entity.id, component, finalLevel, duration, startTime, endTime)
        constructions[constructionId] = construction
        return true
    }

    private fun getId(entity: Entity, component: BuildingComponent): String {
        return "${entity.id}#${component.name}"
    }

    fun processUpdates(): List<ConstructionChangeEvent> {
        // TODO use some internal ticks
        val now = System.currentTimeMillis()

        val (expired, ongoing) = constructions.values.partition { it.endTimestamp <= now }
        val expiredEvents = expired.mapNotNull{createEvent(it,  false, 0)}
        val ongoingEvents = ongoing.mapNotNull{createEvent(it,  true, it.endTimestamp - now)}

        expired.forEach { handleFinished(it) }
        return expiredEvents + ongoingEvents
    }

    private fun handleFinished(construction: Construction) {
        construction.component.level = construction.finalLevel
        constructions.remove(construction.id)
    }

    private fun createEvent(construction: Construction, ongoing: Boolean, remainingMillis: Long): ConstructionChangeEvent? {
        val entity = entityManager.getEntity(construction.entityId) ?: return null
        return ConstructionChangeEvent(entity, construction.component, construction.finalLevel, ongoing, remainingMillis)
    }
}
