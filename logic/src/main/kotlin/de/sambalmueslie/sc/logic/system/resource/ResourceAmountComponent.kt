package de.sambalmueslie.sc.logic.system.resource

import de.sambalmueslie.sc.logic.ecs.common.Component

data class ResourceAmountComponent(
    val resourceName: String,
    var amount: Int
) : Component
