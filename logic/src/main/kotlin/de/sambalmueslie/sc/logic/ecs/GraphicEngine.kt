package de.sambalmueslie.sc.logic.ecs

interface GraphicEngine {
    fun render()
}
