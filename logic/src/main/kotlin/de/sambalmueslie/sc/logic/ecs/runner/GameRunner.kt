package de.sambalmueslie.sc.logic.ecs.runner


import de.sambalmueslie.sc.logic.ecs.GraphicEngine
import de.sambalmueslie.sc.logic.ecs.engine.LogicEngine
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.lang.Thread.sleep
import kotlin.system.measureTimeMillis

class GameRunner(
    private val logicEngine: LogicEngine,
    private val graphicEngine: GraphicEngine,
    fps: Int = 100,
    private val maxAcc: Long = 200,
    private val waitAfterProcessing: Boolean = true
) : Runnable {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(GameRunner::class.java)
    }

    private val dt = 1000L / fps
    private var running = true

    fun stop() {
        logger.info("Stop game")
        this.running = false
    }

    override fun run() {
        logger.info("Start game")
        var acc = 0L
        var frameStart = System.currentTimeMillis()
        while (running) {
            logger.trace("Start frame cycle at $frameStart")
            val currentTime = System.currentTimeMillis()
            acc += currentTime - frameStart
            frameStart = currentTime

            val duration = measureTimeMillis {
                if (acc > maxAcc) {
                    acc = maxAcc
                }

                while (acc > dt) {
                    logicEngine.process(dt)
                    acc -= dt
                }
                graphicEngine.render()
            }
            logger.info("Processing finished within $duration ms.")
            if (waitAfterProcessing) {
                waitAfterProcessing(duration)
            }
        }
    }

    private fun waitAfterProcessing(duration: Long) {
        val pause = dt - duration
        if (pause > 0) {
            sleep(pause)
        }
    }


}
