package de.sambalmueslie.sc.logic.system.common

import kotlin.math.pow


class Expansion(private val a: Double, private val b: Double) {

    fun getValue(x: Int): Double {
        return a * b.pow(x.toDouble())
    }

}
