package de.sambalmueslie.sc.logic.system.movement

import de.sambalmueslie.sc.logic.ecs.common.Entity
import de.sambalmueslie.sc.logic.ecs.event.EntityChangeEvent

data class PositionChangeRequest(
    override val entity: Entity,
    val change: List<Int>,
    val movable: Boolean = false,
    val initial: Boolean = false
) : EntityChangeEvent
