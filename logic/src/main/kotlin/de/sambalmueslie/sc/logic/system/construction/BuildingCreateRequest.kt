package de.sambalmueslie.sc.logic.system.construction

import de.sambalmueslie.sc.logic.ecs.common.Entity
import de.sambalmueslie.sc.logic.ecs.event.EntityChangeEvent
import de.sambalmueslie.sc.logic.system.common.Expansion

data class BuildingCreateRequest(
    override val entity: Entity,
    val buildingName: String,
    val minLevel: Int = 0,
    val maxLevel: Int = 30,
    val expansion: Expansion = Expansion(5.0, 1.5)
) : EntityChangeEvent
