package de.sambalmueslie.sc.logic.ecs.event

interface ChangeEventPublisher<T: ChangeEvent> {
    fun publish(event: T)
}
