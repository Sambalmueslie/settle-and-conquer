package de.sambalmueslie.sc.logic.ecs.entity

import de.sambalmueslie.sc.logic.ecs.common.Component
import de.sambalmueslie.sc.logic.ecs.common.Entity
import kotlin.reflect.KClass

interface EntityManager {
    fun createEntity(name: String, components: List<Component> = emptyList()): Entity
    fun getEntity(entityId: Long): Entity?
    fun getAll(): List<Entity>

    fun <T : Component> getComponents(entityId: Long, type: KClass<T>): List<T>
    fun <T : Component> getComponents(entity: Entity, type: KClass<T>): List<T>

    fun <T : Component> addComponent(entityId: Long, component: T): T?
    fun <T : Component> addComponent(entity: Entity, component: T): T?

    fun <T : Component> addComponents(entityId: Long, components: List<T>): List<T>
    fun <T : Component> addComponents(entity: Entity, components: List<T>): List<T>
}
