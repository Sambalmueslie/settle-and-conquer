package de.sambalmueslie.sc.logic.system.movement

import de.sambalmueslie.sc.logic.ecs.common.Entity
import de.sambalmueslie.sc.logic.ecs.event.ChangeEvent
import de.sambalmueslie.sc.logic.ecs.event.EntityChangeEvent

data class PositionChangeEvent(
    override val entity: Entity,
    val component: PositionComponent,
    val formerPosition: List<Int>
) : EntityChangeEvent
