package de.sambalmueslie.sc.logic.main


import de.sambalmueslie.sc.logic.ecs.common.System
import de.sambalmueslie.sc.logic.ecs.event.ChangeEvent
import de.sambalmueslie.sc.logic.ecs.event.ChangeEventPublisher
import de.sambalmueslie.sc.logic.ecs.event.ChangeEventRegistrar
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class SamplePublishingSystem : System {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(SamplePublishingSystem::class.java)
    }

    private lateinit var publisher: ChangeEventPublisher<SampleEvent>
    private var ctr = 0L

    override fun setup(registrar: ChangeEventRegistrar) {
        publisher = registrar.getPublisher(SampleEvent::class)
    }

    override fun process(dt: Long) {
        ctr += 1000 / dt
        if(ctr > 10){
            logger.info("Publish event")
            publisher.publish(SampleEvent())
            ctr = 0
        }
    }

}
