package de.sambalmueslie.sc.logic.system.common

import de.sambalmueslie.sc.logic.ecs.event.ChangeEvent


class SystemEventProcessorCallback<T: ChangeEvent>: (T) -> Boolean {
    override operator fun invoke(event: T): Boolean = TODO()
}
